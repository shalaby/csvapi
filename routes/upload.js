const express = require('express');
const router = express.Router();
const fs = require('fs');
const http = require('follow-redirects').https;
const DOWNLOAD_DIR = './uploads/';

/* GET users listing. */
router.route('/')
    .post(function (req, res) {
      let file_url = req.body.link;
      let download_url = file_url;
      if (file_url.includes('google')) {
        const fileId = file_url.replace('https://drive.google.com/open?id=', '');
        download_url = 'https://drive.google.com/uc?export=download&id=' + fileId;
      }

      const download = function(link) {

        const file_name = Date.now() + '.jpg';
        const file = fs.createWriteStream(DOWNLOAD_DIR + file_name);

        http.get(link, function(response) {
          response.on('data', function(data) {
            file.write(data);
          }).on('end', function() {
            file.end();
            console.log(file_name + ' downloaded to ' + DOWNLOAD_DIR);
            return res.status(200).send(file_name);
          }).on('error', (e) => {
            console.error(`Got error: ${e.message}`);
            return res.status(422).send(e)
          });
        });
      };

      download(download_url);
    });

module.exports = router;
